  $( function() {
		$( ".custom-handle", $(this) ).css("background-color", 'rgb(255,0,0)');
  var gradient = [
		[
			0,
			[255,0,0]
		],
		[
			50,
			[255,255,0]
		],
		[
			100,
			[0,128,0]
		]
	];

	
	var init_value = 0;
	// draw linear color radial gauge
	function drawRadialGauge(actualData, iidd){
		var rawData = actualData,
		data = getData(rawData);
		  function getData(rawData) {
			var data = [],
			  start = Math.round(Math.floor(rawData / 10) * 10);
			data.push(rawData);
			return data;
		  }

		  Highcharts.chart(iidd, {
			chart: {
				type: 'gauge',
				spacing: [0,0,0,0],
        plotBackgroundColor: null,
        plotBackgroundImage: null,
        plotBorderWidth: 0,
				plotShadow: false,
				plotBorderColor:"#D6F3FF",
				plotBackgroundColor:"#D6F3FF"
			},
			title: {
        text: ''
    	},
			tooltip: {
			  enabled: false
			},
			credits: {
			  enabled: false
			},
			pane: {
        startAngle: -90,
        endAngle: 90,
        background: [{
					borderWidth: 0,
					backgroundColor: 'transparent'
				}]
    	},

			yAxis: {
				min: 0,
				max: 100,
				borderWidth: 0,
				showFirstLabel:false,
        showLastLabel:false,
				minorTickWidth: 0,

				tickWidth: 0,
				tickLength: 0,
				tickColor:"#D6F3FF",
				lineWidth: 0,
				labels: {
						step: 5,
						rotation: 'auto'
				},
				title: {
				},
				plotBands:(function () {
					var data = [],
							i, r,g,b;
					for (i = 0; i < 10; i = i + 1) {
						if (i<5){
							r=255;
							g=Math.floor((0+255)*i/5);
							b=0;
						} else {
							r=(0+255)*(9-i)/5;
							g=Math.floor((255-128)*(9-i)/5+128);
							b=0
						}
						if (i == 9){
							data.push({
								from: (10-i)+i*10,
								to:(i+1)*10,
								color: 'rgb(0,128,0)'
							})
						}else{
							data.push({
								from: (9-i)+i*10,
								to:(i+1)*10,
								color: 'rgb('+r+','+g+','+b+')'
							})
						}
					}
					return data;
			}())
			},
			
			series: [{
				animation: false,
				dataLabels: {
					enabled: false
				},
				borderWidth: 0,
			  data: data
			}]
		});
	}

	$(".slider-custom").each(function(){
		var $this = $(this);

		var handle = $( ".custom-handle", $this );
		var slider_text = $( ".slider-text", $this );
		var sliderWidth = $(".color-slider", $this).width();
		drawRadialGauge(init_value, $this[0].id+'-gauge');

		//insert ruler to the slider gauge
		$(".color-slider", $this).slider({ 
			max: 10
		})
		.slider("pips", {
			rest: "label"
		});
		// slider option & event 
		$( ".color-slider", $this ).slider({
				value:init_value,
				min: 0,
				max:10,
				step:0.1,
			slide: function( event, ui ) {
				sliderWidth = $(".color-slider", $this).width();
				var ui_value;
				var colorRange = [];
				ui_value=ui.value*10;
				$.each(gradient, function( index, value ) {
					if(ui_value<=value[0]) {
						colorRange = [index-1,index]
						return false;
					}
				});
				//Get the two closest colors
				var firstcolor = gradient[colorRange[0]][1];
				var secondcolor = gradient[colorRange[1]][1];
				
				//Calculate ratio between the two closest colors
				var firstcolor_x = sliderWidth*(gradient[colorRange[0]][0]/100);
				var secondcolor_x = sliderWidth*(gradient[colorRange[1]][0]/100)-firstcolor_x;
				var slider_x = sliderWidth*(ui_value/100)-firstcolor_x;
				var ratio = slider_x/secondcolor_x
				
				//Get the color with pickHex(thx, less.js's mix function!)
				var result = pickHex( secondcolor,firstcolor, ratio );
				slider_text.text( ui.value );
				handle.css( "background-color", 'rgb('+result[0]+','+result[1]+','+result[2]+')');	
				handle.css( "background-color", 'rgb('+result.join()+')');	
				drawRadialGauge(ui_value, $this[0].id+'-gauge');
			}
		});
	});

	function pickHex(color1, color2, weight) {
		var p = weight;
		var w = p * 2 - 1;
		var w1 = (w/1+1) / 2;
		var w2 = 1 - w1;
		var rgb = [Math.round(color1[0] * w1 + color2[0] * w2),
			Math.round(color1[1] * w1 + color2[1] * w2),
			Math.round(color1[2] * w1 + color2[2] * w2)];
		return rgb;
	}
});