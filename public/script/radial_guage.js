// draw linear color radial gauge
function drawRadialGauge(actualData, iidd){
  var rawData = actualData,
  data = getData(rawData);
    function getData(rawData) {
    var data = [],
      start = Math.round(Math.floor(rawData / 10) * 10);
    data.push(rawData);
    return data;
    }

    var border_color;
    if (actualData < 60)
      border_color = "#FF9C99";
    else if (actualData < 90)
      border_color = "#FFBB21";
    else
      border_color = "#A2FFA2";
    
    Highcharts.chart(iidd, {
    chart: {
      type: 'gauge',
      spacing: [0,0,0,0],
      plotBackgroundColor: null,
      plotBackgroundImage: null,
      plotBorderWidth: 0,
      plotShadow: false,
      plotBorderColor:"#ffffff",
      plotBackgroundColor:"#ffffff"
    },
    title: {
      text: ''
    },
    tooltip: {
      enabled: false
    },
    credits: {
      enabled: false
    },
    pane: {
      startAngle: -90,
      endAngle: 90,
      background: [{
        borderWidth: 5,
        borderColor: border_color,
        backgroundColor: 'transparent'
      }]
    },

    yAxis: {
      min: 0,
      max: 100,
      borderWidth: 0,
      showFirstLabel:false,
      showLastLabel:false,
      minorTickWidth: 0,

      tickWidth: 0,
      tickLength: 0,
      tickColor:"#D6F3FF",
      lineWidth: 0,
      labels: {
          step: 5,
          rotation: 'auto'
      },
      title: {
      },
      plotBands:(function () {
        var data = [],
            i, r,g,b;
        for (i = 0; i < 10; i = i + 1) {
          if (i<5){
            r=255;
            g=Math.floor((0+255)*i/5);
            b=0;
          } else {
            r=(0+255)*(9-i)/5;
            g=Math.floor((255-128)*(9-i)/5+128);
            b=0
          }
          if (i == 9){
            data.push({
              from: (10-i)+i*10,
              to:(i+1)*10,
              color: 'rgb(0,128,0)'
            })
          }else{
            data.push({
              from: (9-i)+i*10,
              to:(i+1)*10,
              color: 'rgb('+r+','+g+','+b+')'
            })
          }
        }
        return data;
    }())
    },
    
    series: [{
      animation: false,
      dataLabels: {
        enabled: false
      },
      borderWidth: 0,
      data: data
    }]
  });
}

function ajax_change(){
  var radial_value = parseInt($("#radial_value").val());
  var radial_id = $("#radial_id");
  var bg_color;
    if (radial_value < 60)
      bg_color = "#FF9C99";
    else if (radial_value < 90)
      bg_color = "#FFBB21";
    else
      bg_color = "#A2FFA2";
  
  var arrow_width = $('#arrow_width').width();
  $("#dial_text").text(radial_value+"%");
  $(".circle-section").css('background-color', bg_color);
  drawRadialGauge(radial_value, 'radial_id');

  $(".arrow").css('width', arrow_width+7);
  $(".line").css('width', arrow_width+7-30);  
  $(".line").css('background', bg_color);
  $(".point").css('border-left', '30px solid '+bg_color);
  
  $(window).resize(function(){
    var arrow_width = $('#arrow_width').width();
    $(".arrow").css('width', arrow_width+7);
    $(".line").css('width', arrow_width+7-30);  
  });
}