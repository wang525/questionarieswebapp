// This is the payment method defination.
module.exports = {
  'payment': [{
    'method':'Individual',
    'price':29.95,
    'number_master': 1,
    'number_users': 5,
  },
   {
    'method':'Enterprise',
    'price':299.95,
    'number_master': 2,
    'number_users': 100,
  },
   {
    'method':'Custom',
    'price':'plan',
    'number_master': 50,
    'number_users': 'unlimited',
  }]
};
