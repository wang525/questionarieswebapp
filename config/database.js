// This is an example database config file. Replace these presets with your database info and add this file to your .gitignore.
module.exports = {
  'connection': {
    'host': 'localhost',
    'user': 'root',
    'password': 'staralliance'
  },
  'database': 'project_test',
  'socketPath': '/var/run/mysqld/mysqld.sock',
  "connectTimeout": 20000,
  "acquireTimeout": 20000
};
