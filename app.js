var bodyParser   = require('body-parser');
var cookieParser = require('cookie-parser');
var express      = require('express');
var exphbs       = require('express-handlebars');
var favicon      = require('serve-favicon');
var flash        = require('connect-flash');
var logger       = require('morgan');
var path         = require('path');
var session      = require('express-session');
var pdf          = require('express-pdf');
var mailer       = require('express-mailer');


var app = express();

// View engine setup
app.set('views', path.join(__dirname, 'views'));
app.engine('.hbs', exphbs({extname: '.hbs'}));
app.set('view engine', '.hbs');

// Set up favicon, logging, parsing, static files
// Uncomment after placing your favicon in public/images/
//app.use(favicon(path.join(__dirname, 'public', 'images', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//session usage
app.use(session({
  secret: 'projectsecret',
  resave: false,
  saveUninitialized: false
}));
app.use(flash());

// use images directory to all the view files
app.use(express.static('img'));

//generate pdf
app.use(pdf);

//send an email
mailer.extend(app, {
  debug: true,
  from: 'workforce_team@outlook.com',
  host: 'smtp.live.com', // hostname 
  secureConnection: false, // use SSL 
  port: 587, // port for secure SMTP 
  transportMethod: 'SMTP', // default is SMTP. Accepts anything that nodemailer accepts 
  auth: {
    user: 'workforce_team@outlook.com',
    pass: 'mesadmin1234'
  }
});

// Set up routes in configure
require('./routes/route.js')(app);
// set up the payment through braintree
require('./routes/pay.js')(app);
// Set up the code generation of master code and user code
require('./routes/plan.js')(app);
// Set up the management of master code
require('./routes/master.js')(app);
// Set up the management of user code
require('./routes/user.js')(app);

// Catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// Error handlers

// Development error handler
// Will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// Production error handler
// No stacktraces leaked to user
app.use(function(err, req, res) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;
