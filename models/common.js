


// create array size function
var createArray = function(length) {
  var arr = new Array(length || 0),
      i = length;

  if (arguments.length > 1) {
      var args = Array.prototype.slice.call(arguments, 1);
      while(i--) arr[length-1 - i] = createArray.apply(this, args);
  }

  return arr;
}
var roundToTwo = function (num) {    
  return +(Math.round(num + "e+2")  + "e-2");
}
var changeColor = function (result) {    
  var color;
  if (result < 6.0)
    color = '#FF9C99';
  else if (result < 9.0)
    color = '#FFBB21';
  else 
    color = '#A2FFA2';

  return color;
}
exports.createArray=createArray;
exports.roundToTwo=roundToTwo;
exports.changeColor=changeColor;