var db     = require('./db');
var common = require('./common');
var ShortUniqueId = require('short-unique-id');

// Instantiate 
var uid = new ShortUniqueId();

// Create User when press the plan buy button 
var createUser=function(req, res, callback) {

  var NumberOfMaster  = req.query.NumberOfMaster;
  var NumberOfUser    = req.query.NumberOfUser;
  var SessionID       = req.sessionID;

  var Users =  common.createArray(NumberOfUser, 2);
  var sql = "INSERT INTO user ( code, link_id ) values ?";
  
  for(var i = 0; i < NumberOfUser; i++) {
    Users[i][0] = uid.randomUUID(10);
    Users[i][1] = SessionID;
  }
  
  db.query(sql, [Users], function(err) {
      if (err) {
        if (err.code === 'ER_DUP_ENTRY') {
          // If we somehow generated a duplicate sessionLink NumberOfMaster, try again
          console.log(err);
        }
        return callback('error');
      }
      // Successfully created Users
      return callback(null, Users);
    }
  );
};
var readUser=function(req, res, callback) {
  
  var master_code  = req.query.master_code;

  var sql1 = 'SELECT link_id FROM master WHERE code = ?';
  var sql2 = 'SELECT code FROM user WHERE link_id = ?';

  db.query(sql1, [master_code], function(err, link_id) {
    if (err)
      return callback(err);

    if (link_id.length) {
      db.query(sql2, [link_id[0].link_id], function(err, code) {
        if (err)
          return callback(err);
  
        if (code.length) {
          return callback(null, code, link_id);
        } else {
          return callback(err);
        }
      });
    } else {
      return callback(err);
    }
  });
};
var verifyUser=function(req, res, callback) {
  
  var user_code = req.body.code;

  var sql1 = 'SELECT link_id FROM user WHERE code = ?';
  var sql2 = 'SELECT position FROM master WHERE link_id = ?';
  
  db.query(sql1, [user_code], function(err, link_id) {
    if (err) {
    console.log(err);
      return callback(err);
    }
    if (link_id.length) {
      console.log("read position");
      db.query(sql2, [link_id[0].link_id], function(err, position) {
        if (err)
          return callback(err);
  
        if (position.length) {
          return callback(null, link_id, position);
        } else {
          return callback(err);
        }
      });
    } else {
      return callback(err);
    }
  });
};
exports.createUser=createUser;
exports.readUser=readUser;
exports.verifyUser=verifyUser;