var db     = require('./db');
var common = require('./common');
var ShortUniqueId = require('short-unique-id');

// Instantiate 
var uid = new ShortUniqueId();

// Create Master when press the plan buy button 
var createMaster=function(req, res, callback) {

  var NumberOfMaster  = req.query.NumberOfMaster;
  var NumberOfUser    = req.query.NumberOfUser;
  var SessionID       = req.sessionID;
  var company_name    = req.session.company_name;
  var email           = req.session.email;

  var Masters = common.createArray(NumberOfMaster, 5);
  var sql = 'INSERT INTO master ( code, status, link_id, email, position ) values ?';

  for(var i = 0; i < NumberOfMaster; i++) {
    Masters[i][0] = uid.randomUUID(7);        // code of master table
    Masters[i][1] = 'true';                   // status of master table
    Masters[i][2] = SessionID;                // SessionID of master table from Link table 
    Masters[i][3] = email;                    // email of master table
    Masters[i][4] = company_name;             // position of master table
    
  }

  db.query(sql, [Masters], function(err) {
      if (err) {
        if (err.code === 'ER_DUP_ENTRY') {
          // If we somehow generated a duplicate sessionLink NumberOfMaster, try again
          console.log(err);
        }
        return callback('error');
      }
      // Successfully created link
      return callback(null, Masters);
    }
  );
};

var readMaster=function(req, res, callback) {
  
  var master_code  = req.query.master_code;

  var sql1 = 'SELECT link_id FROM master WHERE code = ?';
  var sql2 = 'SELECT code FROM master WHERE link_id = ?';

  db.query(sql1, [master_code], function(err, link_id) {
    if (err)
      return callback(err);

    if (link_id.length) {
      db.query(sql2, [link_id[0].link_id], function(err, code) {
        if (err)
          return callback(err);
  
        if (code.length) {
          return callback(null, code);
        } else {
          return callback(err);
        }
      });
    } else {
      return callback(err);
    }
  });
};
var verifyMaster=function(req, res, callback) {
  
  var master_code = req.body.code;

  var sql = 'SELECT link_id, position FROM master WHERE code = ?';

  db.query(sql, [master_code], function(err, row) {
    if (err)
      return callback(err);

    if (row.length) {
      return callback(null, row);
    } else {
      return callback(err);
    }
  });
};
exports.createMaster=createMaster;
exports.readMaster=readMaster;
exports.verifyMaster=verifyMaster;