var db     = require('./db');
var common = require('./common');
var dateTime = require('node-datetime');

var createResultUser=function(userCode, op, mark, avg_mark, count, link_id, callback) {
  
  var dt = dateTime.create();
  var currentDate = dt.format('Y/m/d H:M');
  
  var sql = 'INSERT INTO result ( user_code, avg_res, json, date, op1, op2, op3, op4, op5, op6, op7, op8, link_id ) values (?,?,?,?,?,?,?,?,?,?,?,?,?)';
  db.query(sql, [userCode, avg_mark, mark, currentDate, op[0], op[1], op[2], op[3], op[4], op[5], op[6], op[7], link_id ], function(err) {
    if (err)
      return callback(err);
    console.log("error show", err); 
    if (!err) {
      console.log("You have just register your answers");
      return callback(null);
    } 
  });
};

var updateResultUser=function(userCode, op, mark, avg_mark, count, link_id, callback) {
  
  var dt = dateTime.create();
  var currentDate = dt.format('Y/m/d H:M');

  console.log("$$$$$$");
  
  var sql = 'UPDATE result SET avg_res=?, json=?, date=?, op1=?, op2=?, op3=?, op4=?, op5=?, op6=?, op7=?, op8=? where user_code=?';

  db.query(sql, [avg_mark, mark, currentDate, op[0], op[1], op[2], op[3], op[4], op[5], op[6], op[7], userCode ], function(err, res_update) {
    if (err)
      return callback(err);
      
    if (!err) {
      console.log("You have just register your answers");
      return callback(null);
    } 
  });
};


var verifyResultUser=function(userCode, op, mark, avg_mark, count, link_id, callback) {
  
  var sql = 'SELECT user_code FROM result WHERE user_code = ?';

  db.query(sql, [userCode], function(err, res_user) {
    if (err)
      return callback(err);

    if (res_user.length) {
      return updateResultUser(userCode, op, mark, avg_mark, count, link_id, callback);      
    } else {
      return createResultUser(userCode, op, mark, avg_mark, count, link_id, callback);      
    }
  });
};

exports.verifyResultUser=verifyResultUser;
