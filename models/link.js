var db     = require('./db');
var common = require('./common');
var bcrypt = require('bcrypt-nodejs');
var uuidV4 = require('uuid/v4');

// Gets a random session id for this master
var generateSessionId = function() {
  return uuidV4();
};

// Hash and salt the password with bcrypt
var hashSessionID = function(sessionID) {
  return bcrypt.hashSync(sessionID, bcrypt.genSaltSync(8), null);
};

// Check if sessionID is correct
var validSessionID = function(sessionID, savedSessionID) {
  return bcrypt.compareSync(sessionID, savedSessionID);
};

// Create Link when press the plan buy button 
var createLink=function(req, res, callback) {

  var NumberOfMaster  = req.query.NumberOfMaster;
  var NumberOfUser    = req.query.NumberOfUser;
  var SessionID = generateSessionId();
  
  var Link = common.createArray(1,3);
  var sql = "INSERT INTO link ( session_link, number_master, number_user ) values ?";

  Link[0][0] = hashSessionID(SessionID);
  Link[0][1] = NumberOfMaster;
  Link[0][2] = NumberOfUser;

  req.sessionID = hashSessionID(SessionID);
  
  db.query(sql, [Link], function(err) {
      if (err) {
        if (err.code === 'ER_DUP_ENTRY') {
          // If we somehow generated a duplicate sessionLink NumberOfMaster, try again
          console.log(err);
        }
        console.log("Link err", err);
        return callback('error');
      }
      // Successfully created link
      return callback(null, Link);
    }
  );
};

exports.createLink=createLink;