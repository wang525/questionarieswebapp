var db     = require('./db');
var common = require('./common');
var dateTime = require('node-datetime');

function roundToTwo(num) {    
  return +(Math.round(num + "e+2")  + "e-2");
}

var createResultMaster=function(link_id, master_code, filter_creteria, callback) {

  var filter=[];
  var i = 0, j=0;
  var avg_value = 0.0;

  var dt = dateTime.create();
  var currentDate = dt.format('Y/m/d   H:M');

  var filter_sql=[];
  var sql1 = 'SELECT avg_res, json, date FROM result WHERE link_id =?';
  var number_users;

  filter_sql[0]=link_id;

  for (i=0; i<filter_creteria.length; i++){
    if (filter_creteria[i] == "none"){
      filter[i] = 0;
    } else {
      sql1 += ' AND op'+(i+1)+'=?';
      filter_sql[j+1] = filter_creteria[i];
      j = j+1;
      filter[i] = filter_creteria[i];
    }
  }
  
  //var sql1 = 'SELECT avg_res, json, date FROM result WHERE link_id =? AND op1 =? AND op2=? AND op3=? AND op4=? AND op5 =? AND op6=? AND op7=? AND op8=?';
  var sql2 = "INSERT INTO result_master ( master_id, op1, op2, op3, op4, op5, op6, op7, op8, result, number_users, date ) values (?,?,?,?,?,?,?,?,?,?,?,?)"

  console.log("sql1", sql1);
  console.log("link_id", link_id);
  console.log("after for server", filter_sql);
  
  db.query(sql1, filter_sql, function(err, res_data) {    
    if (err) {
      console.log("err");
        return callback(err);
      }
      if (res_data.length) {
        console.log("res_data show", res_data);
        // calculate average value
        for (i=0; i<res_data.length; i++){
          avg_value = avg_value + roundToTwo(parseFloat(res_data[i].avg_res))
        }
        avg_value = roundToTwo(avg_value / res_data.length);
        number_users = res_data.length;
        db.query(sql2, [master_code, filter_creteria[0], filter_creteria[1], filter_creteria[2], filter_creteria[3], filter_creteria[4], filter_creteria[5], filter_creteria[6], filter_creteria[7], avg_value, number_users, currentDate], function(err) {
          if (err){
            return callback(err);
          }
          if (!err) {
            console.log("You have just register your master answers");
            return callback(null, res_data);
          } 
        });
      } else {
        return callback(err);
      }
    }
  );
};

var readResultMaster=function(master_code, callback) {

  var sql = "SELECT id, master_id, op1, op2, op3, op4, op5, op6, op7, op8, result, number_users, date FROM result_master WHERE master_id =?"

  db.query(sql, [master_code], function(err, read_data) {    
    if (err) {
      console.log("err", err);
        return callback(err);
      }
      if (read_data.length) {
        console.log("read_data", read_data);
        return callback(null, read_data);      
      } else {
        console.log("none error but no data");
        return callback(err);
      }
    }
  );
};

var filterResultMaster=function(master_code, result_id, callback) {
  
  var sql = "SELECT master_id, op1, op2, op3, op4, op5, op6, op7, op8, result, number_users, date FROM result_master WHERE master_id =? AND id=?";
  
  db.query(sql, [master_code, result_id], function(err, read_data) {    
    if (err) {
      console.log("err", err);
        return callback(err);
      }
      if (read_data.length) {
        console.log("read_data", read_data);
        return callback(null, read_data);      
      } else {
        console.log("none error but no data");
        return callback(err);
      }
    }
  );
};

  
var verifyResultUser=function(userCode, op, mark, avg_mark, count, callback) {
  
  
};

exports.createResultMaster=createResultMaster;
exports.readResultMaster=readResultMaster;
exports.filterResultMaster=filterResultMaster;


