var db     = require('./db');
var common = require('./common');


// Create User when press the plan buy button 
var createQuestion=function(req, res, callback) {

  var Questions =  common.createArray(NumberOfUser, 3);
  var sql = "INSERT INTO question ( question, min, max ) values ?";
  
  for(var i = 0; i < NumberOfUser; i++) {
    Questions[i][0] = ' ';
    Questions[i][1] = ' ';
    Questions[i][2] = ' ';
  }
  
  db.query(sql, [Questions], function(err) {
      if (err) {
        if (err.code === 'ER_DUP_ENTRY') {
          // If we somehow generated a duplicate sessionLink NumberOfMaster, try again
          console.log(err);
        }
        return callback('error');
      }
      // Successfully created Users
      return callback(null, Questions);
    }
  );
};
var readQuestion=function(req, res, callback) {
  
  var sql = 'SELECT question, min, max FROM question';
  
  db.query(sql, [], function(err, questions) {
    if (err)
      return callback(err);

    if (questions.length) {
      return callback(null, questions);
    } else {
      return callback(err);
    }
  });
};

exports.createQuestion=createQuestion;
exports.readQuestion=readQuestion;
