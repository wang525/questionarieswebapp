
module.exports = function(app) {
  var braintree = require('braintree');
  var gateway = require('../config/gateway');
  var payment_method = require('../config/payment_method');
  
  var TRANSACTION_SUCCESS_STATUSES = [
    braintree.Transaction.Status.Authorizing,
    braintree.Transaction.Status.Authorized,
    braintree.Transaction.Status.Settled,
    braintree.Transaction.Status.Settling,
    braintree.Transaction.Status.SettlementConfirmed,
    braintree.Transaction.Status.SettlementPending,
    braintree.Transaction.Status.SubmittedForSettlement
  ];
  
  function formatErrors(errors) {
    var formattedErrors = '';

    for (var i in errors) { // eslint-disable-line no-inner-declarations, vars-on-top
      if (errors.hasOwnProperty(i)) {
        formattedErrors += 'Error: ' + errors[i].code + ': ' + errors[i].message + '\n';
      }
    }
    return formattedErrors;
  }

  function createResultObject(transaction) {
    var result;
    var status = transaction.status;
    console.log("status:", TRANSACTION_SUCCESS_STATUSES.indexOf(status));
    console.log("status222:", status);

    if (TRANSACTION_SUCCESS_STATUSES.indexOf(status) !== -1) {
      result = {
        header: 'Sweet Success!',
        success: 'success',
        message: 'Your test transaction has been successfully processed.'
      };
    } else {
      result = {
        header: 'Transaction Failed',
        failed: 'failed',
        message: 'Your test transaction has been failed. Please try again.'
      };
    }

    return result;
  }
  
  app.get('/plan/:id/:method', function (req, res, next) {
    var result;
    var transactionId = req.params.id;
    var method = req.params.method;

    gateway.transaction.find(transactionId, function (err, transaction) {
      result = createResultObject(transaction);
      console.log("transaction.status", transaction.status);
      if (transaction.status==='processor_declined' || (result.success)){
        res.redirect('/plan_buy?NumberOfMaster='+payment_method.payment[method].number_master+
                                   '&NumberOfUser='+payment_method.payment[method].number_users);
      } else {
        console.log("failed!!!!");        
        res.redirect('/');    
      }

    });
  });
  
  app.post('/pay', function (req, res, next) {
    var transactionErrors;
    var amount = req.body.amount; // In production you should not take amounts directly from clients
    var nonce = req.body.payment_method_nonce;
    var method = req.body.method;
    var email = req.body.email;
    var company_name = req.body.company_name;
    var organization_type = req.body.organization_type;
    var organization_size = req.body.organization_size;

    req.session.company_name = company_name;
    req.session.organization_type = organization_type;
    req.session.organization_size = organization_size;
    req.session.email = email;
    
    gateway.transaction.sale({
      amount: amount,
      paymentMethodNonce: nonce,
      options: {
        submitForSettlement: true
      }
    }, function (err, result) {
      if (result.success || result.transaction) {
        res.redirect('/plan/' + result.transaction.id + '/' + method);
      } else {
        transactionErrors = result.errors.deepErrors();
        res.redirect('/pay');
      }
    });
  });

};
