var link=require('../models/link');
var master=require('../models/master');
var user=require('../models/user');
var common=require('../models/common');
var question=require('../models/questions');
var resultUser=require('../models/result_user');
var resultMaster=require('../models/result_master');
var dateTime = require('node-datetime');

// manage users and masters
module.exports = function(app) {

  app.get('/master_welcome', function(req, res, next) {
    
    var current_master_code = req.query.master_code;
    var firm_name = req.session.company_name;

    res.render('master/master_welcome', {master_code:current_master_code, firm_name:firm_name});

  });

  app.get('/master_role', function(req, res, next) {
    
    var current_master_code = req.query.master_code;
    var firm_name = req.session.company_name;
    var data;
    var res_value;

    master.readMaster(req, res, function(err, master_code){   
      if(master_code){
        user.readUser(req, res, function(err, user_code, link_id){
          if(user_code){
            resultMaster.readResultMaster(current_master_code, function(err, res_data){
              if(!err){
                if (!res_data){
                  data = '';
                  res_value = false;
                } else {
                  res_value = true;
                  data = '<div id="result_pdf" class="row mx-auto" style="padding:10px">';
                  for (i=0; i<res_data.length; i++){
        
                    // if (res_data[i].result < 6.0)
                    //   color = '#FF9C99';
                    // else if (res_data[i].result < 9.0)
                    //   color = '#FFBB21';
                    // else 
                    //   color = '#A2FFA2';
                    
                    color = common.changeColor(res_data[i].result);

                    console.log("result float", common.roundToTwo(res_data[i].result));
                    data += '<div class="col-md-3" style="padding:20px 10px">\
                              <div class="bg-white text-dark-green" style="height:80%;padding:5px 10px; border:2px solid '+color+'">\
                                <p style="font-size:16px; margin:0px">Organization Years:  '+res_data[i].op1+'</p>\
                                <p style="font-size:16px; margin:0px">Job Category:  '+res_data[i].op2+'</p>\
                                <p style="font-size:16px; margin:0px">Current Job Years:  '+res_data[i].op3+'</p>\
                                <p style="font-size:16px; margin:0px">Work Type:  '+res_data[i].op4+'</p>\
                                <p style="font-size:16px; margin:0px">Age:  '+res_data[i].op5+'</p>\
                                <p style="font-size:16px; margin:0px">Gender:  '+res_data[i].op6+'</p>\
                                <p style="font-size:16px; margin:0px">Organization Location:  '+res_data[i].op7+'</p>\
                                <p style="font-size:16px; margin:0px">Employees Type:  '+res_data[i].op8+'</p>\
                              </div>\
                              <div class="text-white" style="background-color:'+color+'">\
                                <div class="row" style="margin:0px">\
                                  <div class="col-md-12">\
                                    <p style="font-size:12px; padding:5px;margin:0px">'+res_data[i].date+'</p>\
                                  </div>\
                                </div>\
                              </div>\
                              <div class="bg-light-green text-white bg_result_pdf" style="background-color:'+color+'">\
                                <div class="row" style="margin:0px">\
                                  <div class="col-5">\
                                    <p style="font-size:20px; padding:5px;margin:0px">Users: '+res_data[i].number_users+'</p>\
                                  </div>\
                                  <div class="col-5 text-right">\
                                    <p style="font-size:20px; padding:5px;margin:0px"><b>'+common.roundToTwo(res_data[i].result)+'</b></p>\
                                  </div>\
                                  <div class="col-2 text-center" style="margin:0px; padding:3px">\
                                  <a href="/generate_pdf?master_code='+res_data[i].master_id+'&cur='+current_master_code+'&id='+res_data[i].id+'">\
                                    <img src="../download.png" class="img-fluid mx-auto" width="40" height="40"></a>\
                                  </div>\
                                </div>\
                              </div>\
                            </div>';
                  }
                  data += '</div>';
                }
                res.render('master/master_role', {current_master_code:current_master_code, 
                                                  master_code: master_code, 
                                                  user_code:user_code, 
                                                  link_id:link_id, 
                                                  firm_name:firm_name,
                                                  res_value:res_value,
                                                  data:data});
              }
            });
          }
        });
      }
    });
  });

  app.post('/build_creteria', function(req, res, next){

    var master_code = req.body.master_code;
    var link_id = req.body.link_id;
    var i = 0;

    var filter_creteria = [];
    filter_creteria[0] = req.body.organization_years;
    filter_creteria[1] = req.body.job_category;
    filter_creteria[2] = req.body.current_job_years;
    filter_creteria[3] = req.body.work_type;
    filter_creteria[4] = req.body.birthday;
    filter_creteria[5] = req.body.gender;
    filter_creteria[6] = req.body.organization_location;
    filter_creteria[7] = req.body.employees_type;

    var data, i, j; 
    var avg_mark = 0;
    var text, color;
    var no_data = '<div class="bg-white mx-auto" style="padding:20px">\
                    <div class="row mx-auto" style="padding:70px 20px">\
                      <div class="col-12 text-center">\
                        There is no data to show.\
                      </div>\
                    </div>\
                  </div>';

    for (i=0; i<filter_creteria.length; i++){
      if(!filter_creteria[i]){
        filter_creteria[i] = 'none';
      }
    }
    console.log("--->");
    resultMaster.createResultMaster(link_id, master_code, filter_creteria, function(err, create_data){
      if(!err){
        if(!create_data){
          data = no_data;
          res.status(200).send(data);
        } else {
          resultMaster.readResultMaster(master_code, function(err, res_data){
            if(!err){
              console.log("data receive successful!");   
              if (!res_data){
                console.log("no data");
                data = no_data;
              } else {
                console.log("yes data");
                data = '<div id="result_pdf" class="row mx-auto" style="padding:10px">';
                for (i=0; i<res_data.length; i++){
      
                  // if (res_data[i].result < 6.0)
                  //   color = '#FF9C99';
                  // else if (res_data[i].result < 9.0)
                  //   color = '#FFBB21';
                  // else 
                  //   color = '#A2FFA2';

                  color = common.changeColor(res_data[i].result);

                  console.log("result float", common.roundToTwo(res_data[i].result));
                  data += '<div class="col-md-3" style="padding:20px 10px">\
                            <div class="bg-white text-dark-green" style="height:80%;padding:5px 10px; border:2px solid '+color+'">\
                              <p style="font-size:16px; margin:0px">Organization Years:  '+res_data[i].op1+'</p>\
                              <p style="font-size:16px; margin:0px">Job Category:  '+res_data[i].op2+'</p>\
                              <p style="font-size:16px; margin:0px">Current Job Years:  '+res_data[i].op3+'</p>\
                              <p style="font-size:16px; margin:0px">Work Type:  '+res_data[i].op4+'</p>\
                              <p style="font-size:16px; margin:0px">Age:  '+res_data[i].op5+'</p>\
                              <p style="font-size:16px; margin:0px">Gender:  '+res_data[i].op6+'</p>\
                              <p style="font-size:16px; margin:0px">Organization Location:  '+res_data[i].op7+'</p>\
                              <p style="font-size:16px; margin:0px">Employees Type:  '+res_data[i].op8+'</p>\
                            </div>\
                            <div class="text-white bg-dark-grey" style="background-color:'+color+'">\
                              <div class="row" style="margin:0px">\
                                <div class="col-md-12">\
                                  <p style="font-size:12px; padding:5px;margin:0px">'+res_data[i].date+'</p>\
                                </div>\
                              </div>\
                            </div>\
                            <div class="bg-light-green text-white bg_result_pdf" style="background-color:'+color+'">\
                              <div class="row" style="margin:0px">\
                                <div class="col-6">\
                                  <p style="font-size:20px; padding:5px;margin:0px">USER:'+res_data[i].number_users+'</p>\
                                </div>\
                                <div class="col-4 text-right">\
                                  <p style="font-size:20px; padding:5px;margin:0px"><b>'+common.roundToTwo(res_data[i].result)+'</b></p>\
                                </div>\
                                <div class="col-2 text-center" style="margin:0px; padding:3px">\
                                  <a href="/generate_pdf?master_code='+res_data[i].master_id+'&cur='+master_code+'&id='+res_data[i].id+'">\
                                    <img src="../download.png" class="img-fluid mx-auto" width="40" height="40"></a>\
                                </div>\
                              </div>\
                            </div>\
                          </div>';
                }
                data += '</div>';
              }
              res.status(200).send(data);
            }
          });
        }
      }
    });
  });

  app.get('/show_report/:code', function(req, res, next) {
    
    var master_code = req.params.code;
    var firm_name = req.session.company_name;
    var data_length;

    resultMaster.readResultMaster(master_code, function(err, res_data){
      if(!err){
        console.log("data receive successful!");  
        if (!res_data){
          data_length = true;
        } else {
          data_length =false;
        }
        console.log("data_length", data_length);
        res.render('master/master_reports', {res_data:res_data, data_length:data_length, firm_name:firm_name});
      }
    });
  });

  /*
  app.post('/master_code', function(req, res, next) {
    var masterCode = req.body.masterCode;
    var firm_name = req.session.company_name;
    var data ='';
    var color;
    console.log("receive from build creteria");
    // var link_id = req.query.link;
    resultMaster.readResultMaster(masterCode, function(err, res_data){
      if(!err){
        console.log("data receive successful!");   
        if (!res_data){
          data = '<div class="col-md-12 text-center" style="padding:20px 10px">\
                    There is no data to show.\
                  </div>';
        } else {
          data = '<div id="result_pdf" class="row mx-auto" style="padding:10px">';
          for (i=0; i<res_data.length; i++){

            // if (res_data[i].result < 6.0)
            //   color = '#FF9C99';
            // else if (res_data[i].result < 9.0)
            //   color = '#FFBB21';
            // else 
            //   color = '#A2FFA2';
            
            color = common.changeColor(res_data[i].result);

            console.log("result float", common.roundToTwo(res_data[i].result));
            data += '<div class="col-md-3" style="padding:20px 10px">\
                      <div class="bg-white text-dark-green border-dark-grey" style="height:80%;padding:5px 10px">\
                        <p style="font-size:16px; margin:0px">Organization Years:  '+res_data[i].op1+'</p>\
                        <p style="font-size:16px; margin:0px">Job Category:  '+res_data[i].op2+'</p>\
                        <p style="font-size:16px; margin:0px">Current Job Years:  '+res_data[i].op3+'</p>\
                        <p style="font-size:16px; margin:0px">Work Type:  '+res_data[i].op4+'</p>\
                        <p style="font-size:16px; margin:0px">Age:  '+res_data[i].op5+'</p>\
                        <p style="font-size:16px; margin:0px">Gender:  '+res_data[i].op6+'</p>\
                        <p style="font-size:16px; margin:0px">Organization Location:  '+res_data[i].op7+'</p>\
                        <p style="font-size:16px; margin:0px">Employees Type:  '+res_data[i].op8+'</p>\
                      </div>\
                      <div class="text-white bg-dark-grey">\
                        <div class="row" style="margin:0px">\
                          <div class="col-md-12">\
                            <p style="font-size:12px; padding:5px;margin:0px">'+res_data[i].date+'</p>\
                          </div>\
                        </div>\
                      </div>\
                      <div class="bg-light-green text-white bg_result_pdf" style="background-color:'+color+'">\
                        <div class="row" style="margin:0px">\
                          <div class="col-3">\
                            <p style="font-size:20px; padding:5px;margin:0px">'+(i+1)+'</p>\
                          </div>\
                          <div class="col-7 text-right">\
                            <p style="font-size:20px; padding:5px;margin:0px"><b>'+common.roundToTwo(res_data[i].result)+'</b></p>\
                            <input class="result_value" type="hidden" value="'+common.roundToTwo(res_data[i].result)+'">\
                          </div>\
                          <div class="col-2" style="margin:0px; padding:3px">\
                            <a href="/generate_pdf?master_code='+res_data[i].master_id+'&o1='+res_data[i].op1+'&o2='+res_data[i].op2+'&o3='+res_data[i].op3+'&o4='+res_data[i].op4+'&o5='+res_data[i].op5+'&o6='+res_data[i].op6+'&o7='+res_data[i].op7+'&o8='+res_data[i].op8+'">\
                              <img src="../download.png" class="img-fluid mx-auto" width="80" height="80"></a>\
                          </div>\
                        </div>\
                      </div>\
                    </div>';
          }
          data += '</div>';
        }
        res.status(200).send(data);
      }
    });
  });
  */

  app.get('/generate_pdf', function(req, res, next) {
    var master_code = req.query.master_code;
    var prev_master_code = req.query.cur;
    var result_id = req.query.id;

    var filter_creteria = [];
    var i=0;
    var j=0;
    var data='';
    var text, color;
    
    var firm_name = req.session.company_name;
    
    console.log("generate pdf wait...");

    resultMaster.filterResultMaster(master_code, result_id, function(err, res_data){
      if(!err){
        if(!res_data){
          data = '<div class="bg-white mx-auto text-center" style="padding:20px">\
                      There is no data to show.\
                  </div>';
        } else {
          avg_mark = parseFloat(res_data[0].result);
          
          if (avg_mark < 6.0){
            text = 'Your organization is in the danger zone. We believe that it needs to work with us and get better before its too late';
          }
          else if (avg_mark < 9.0){
            text = 'Your organization is in the critical position. We believe that it needs to work with us and improve its productivity';
          }
          else {
            text = 'Congratulations!!! You practice ethical values in your organisation marvellously. We are happy to bring forth this survey to you. keep up with the good work';
          }

          console.log("avg_mark", avg_mark);
          console.log("create success!");

          data = '  <div class="bg-white mx-auto" style="padding:20px">\
                      <div class="row">\
                        <div class="col-12 text-dark-green text-center welcome-font" style="padding:20px">\
                          <p> <b> RESULT </b> </p>\
                        </div>\
                      </div>\
                      <div class="row" style="padding-top:50px">\
                        <div class="col-5 col-sm-4 col-md-3 col-lg-2" style="padding:0px">\
                          <div id="radial_id" class="radial-size mx-auto">\
                            <input type="hidden" id="radial_value" value="'+parseInt(avg_mark*10)+'">\
                          </div>\
                        </div>\
                        <div class="col-2 col-sm-4 col-md-1 col-lg-2" id="arrow_width" style="padding:0px">\
                          <div class="arrow">\
                            <div class="line"></div>\
                            <div class="point"></div>\
                          </div>\
                        </div>\
                        <div class="col-5 col-sm-4 col-md-3 col-lg-2" style="padding:5px 0px 0px 0px">\
                          <div class="circle-section mx-auto">\
                            <p id="dial_text" class="text-white text-center result-font"></p>\
                          </div> \
                        </div>\
                        <div class="col-md-5 col-lg-5 mx-auto bg-light-green text-white" style="font-size:16px; padding:10px">\
                          <div class="text-center" style="font-size:20px">\
                            <b>Filter Data</b>\
                          </div>\
                          <div class="row">\
                            <div class="col-7">\
                              <b>Years work for organization</b>\
                            </div>\
                            <div class="col-5">\
                            '+res_data[0].op1+'\
                            </div>\
                          </div>\
                          <div class="row">\
                            <div class="col-7">\
                              <b>Class of Job Category</b>\
                            </div>\
                            <div class="col-5">\
                            '+res_data[0].op2+'\
                            </div>\
                          </div>\
                          <div class="row">\
                            <div class="col-7">\
                              <b>Years of working in current job</b>\
                            </div>\
                            <div class="col-5">\
                            '+res_data[0].op3+'\
                            </div>\
                          </div>\
                          <div class="row">\
                            <div class="col-7">\
                              <b>Type of work arrangement</b>\
                            </div>\
                            <div class="col-5">\
                            '+res_data[0].op4+'\
                            </div>\
                          </div>\
                          <div class="row">\
                            <div class="col-7">\
                              <b>Age</b>\
                            </div>\
                            <div class="col-5">\
                            '+res_data[0].op5+'\
                            </div>\
                          </div>\
                          <div class="row">\
                            <div class="col-7">\
                              <b>Gender</b>\
                            </div>\
                            <div class="col-5">\
                            '+res_data[0].op6+'\
                            </div>\
                          </div>\
                          <div class="row">\
                            <div class="col-7">\
                              <b>Location of the organization</b>\
                            </div>\
                            <div class="col-5">\
                            '+res_data[0].op7+'\
                            </div>\
                          </div>\
                          <div class="row">\
                            <div class="col-7">\
                              <b>Type of employees</b>\
                            </div>\
                            <div class="col-5">\
                            '+res_data[0].op8+'\
                            </div>\
                          </div>\
                        </div>\
                        <div class="col-lg-1"></div>\
                      </div> \
                      <div class="row text-dark-green" style="padding-top:80px">\
                        <div class="col-lg-2"></div>\
                        <div class="col-lg-8">\
                          <div class="text-center subheading-font">\
                            <p>'+text+'</p>\
                          </div>\
                        </div>\
                        <div class="col-lg-2"></div>\
                      </div>\
                    </div>';
        }
        res.render('master/master_reports', {data:data, firm_name: firm_name, prev_master_code:prev_master_code});
      }
    });
    /*
    resultMaster.filterResultMaster(master_code, filter_creteria, function(err, res_data){
      if(res_data){
        console.log("create success!");
          contentPDF='<html>\
                        <body>';
          for (i=0; i<res_data.length; i++){
            json_data=JSON.parse(res_data[i].json);
            contentPDF+=' <div style="font-size:24px; text-align:center; font-weight: bold">\
                            <b>Questions and Answers Result</b>'+i+'\
                            <br>\
                          </div>\
                          <div style="font-size:20px">\
                            <b>Average value</b>: '+res_data[i].avg_res+'\
                            <br>\
                          </div>\
                          <div style="font-size:20px">\
                            <b>Questions and Answers</b>: <br>';
            for (j=0; j<json_data.length; j++){
            contentPDF+=' Questions: '+ json_data[j].question+ '<br>\
                            Answers: '+json_data[j].mark+'<br>';
            }
            contentPDF+=' <div style="font-size:20px">\
                            <b>Date</b>: '+res_data[i].date+'\
                            <br>\
                            <br>\
                          </div>';
          }
            contentPDF+=' </body>\
                        </html>';
        console.log("content pdf");
        //res.render('master/master_test');
        res.pdfFromHTML({
          filename: 'report.pdf',
          htmlContent: contentPDF,
          options: {
            "border": {
              "top": "2in",            // default is 0, units: mm, cm, in, px
              "right": "1in",
              "bottom": "2in",
              "left": "1.5in"
            },
            "header": {
              "height": "25mm",
              "contents": '<div style="font-size: 24px; text-align: center;">Answer and Question Report</div>'
            },
            "footer": {
              "height": "28mm",
              "contents": {
                first: 'Cover page',
                2: 'Second page', // Any page number is working. 1-based index
                default: '<span style="color: #444;">{{page}}</span>/<span>{{pages}}</span>', // fallback value
                last: 'Last Page'
              }
            }
          }
        });
        console.log("end");
      }
    });
    */
  });
}