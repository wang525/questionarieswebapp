var braintree = require('braintree');
var gateway = require('../config/gateway');
var payment_method = require('../config/payment_method');
var link=require('../models/link');
var master=require('../models/master');
var user=require('../models/user');

// Main routes for app
module.exports = function(app) {

  app.get('/', function(req, res, next) {

    gateway.clientToken.generate({}, function (err, response) {
      res.render('landing_page', { clientToken: response.clientToken, payment_method: payment_method });
    });
    
  });

  app.post('/user_step1', function(req, res, next) {
    
    var code = req.body.code;
    
    if(code.length === 7) {
      master.verifyMaster(req, res, function(err, res_data){
        if(res_data){
          req.session.company_name = res_data[0].position;
          res.render('master/master_welcome', {master_code:code, link_id:res_data[0].link_id,firm_name: req.session.company_name});
        } else {
          res.redirect('/');        
        }
      });
    }
    else if (code.length === 10) {
      user.verifyUser(req, res, function(err, link_id, position){
        if(link_id){
          console.log("positin", position);
          req.session.company_name = position[0].position;
          console.log("firm name", req.session.company_name);
          res.render('user/user_step1', {user_code:code, link_id:link_id, firm_name:req.session.company_name});
        } else {
          res.redirect('/');         
        }
      });
    }
    else {
      console.log("landing page againt");
      res.redirect('/');      
    }
  });

  app.get('/master_reports', function(req, res, next) {
    
    res.render('master/master_reports');

  });

};
