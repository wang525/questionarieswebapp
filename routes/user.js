var link=require('../models/link');
var master=require('../models/master');
var user=require('../models/user');
var common=require('../models/common');
var question=require('../models/questions');
var resultUser=require('../models/result_user');
var resultMaster=require('../models/result_master');
var dateTime = require('node-datetime');

module.exports = function(app) {
  app.post('/register_result', function(req, res, next){

    var userCode = req.body.user_code;
    var link_id = req.body.link_id;
    var organization_years = req.body.organization_years;
    var job_category = req.body.job_category;
    var current_job_years = req.body.current_job_years;
    var work_type = req.body.work_type;
    var birthday = req.body.birthday;
    var gender = req.body.gender;
    var organization_location = req.body.organization_location;
    var employees_type = req.body.employees_type;
    var mark = req.body.mark;
    var avg_mark = req.body.avg_mark;
    var count = req.body.count;
    var op =[];

    var dt = dateTime.create();
    var currentDate = dt.format('Y/m/d H:M');

    var interval;
    var birthday_interval;
    var before_age;
    var after_age;
    var text, color;

    op[0] = organization_years;
    op[1] = job_category;
    op[2] = current_job_years;
    op[3] = work_type;
    op[4] = birthday;
    op[5] = gender;
    op[6] = organization_location;
    op[7] = employees_type;

    console.log("mark", avg_mark);

    resultUser.verifyResultUser(userCode, op, mark, avg_mark, count, link_id, function(err){
      if(!err){
        console.log("average mark", parseInt(avg_mark*10));
        var data;
        console.log("this is the ajaz test");
        if (parseFloat(avg_mark) < 6.0){
          color = 'red';
          text = 'Your organization is in the danger zone. We believe that it needs to work with us and get better before its too late';
        }
        else if (parseFloat(avg_mark) < 9.0){
          color = 'yellow';
          text = 'Your organization is in the critical position. We believe that it needs to work with us and improve its productivity';
        }
        else {
          color = 'green';
          text = 'Congratulations!!! You practice ethical values in your organisation marvellously. We are happy to bring forth this survey to you. keep up with the good work';
        }
        data = '  <div class="bg-white mx-auto" style="padding:20px">\
                    <div class="row">\
                      <div class="col-12 text-dark-green text-center welcome-font" style="padding:20px">\
                        <p> <b> RESULT </b> </p>\
                      </div>\
                    </div>\
                    <div class="row" style="padding-top:50px">\
                      <div class="col-4 col-sm-4 col-md-3 col-lg-2" style="padding:0px">\
                        <div id="radial_id" class="radial-size mx-auto">\
                          <input type="hidden" id="radial_value" value="'+parseInt(avg_mark*10)+'">\
                        </div>\
                      </div>\
                      <div class="col-4 col-sm-4 col-md-1 col-lg-2" id="arrow_width" style="padding:0px">\
                        <div class="arrow">\
                          <div class="line"></div>\
                          <div class="point"></div>\
                        </div>\
                      </div>\
                      <div class="col-4 col-sm-4 col-md-3 col-lg-2" style="padding:5px 0px 0px 0px">\
                        <div class="circle-section mx-auto">\
                          <p id="dial_text" class="text-white text-center result-font"></p>\
                        </div> \
                      </div>\
                      <div class="col-md-5 col-lg-5 mx-auto bg-light-green text-white" style="font-size:16px; padding:10px">\
                        <div class="text-center" style="font-size:20px">\
                          <b>Filter Data</b>\
                        </div>\
                        <div class="row">\
                          <div class="col-7">\
                            <b>Years work for organization</b>\
                          </div>\
                          <div class="col-5">\
                          '+organization_years+'\
                          </div>\
                        </div>\
                        <div class="row">\
                          <div class="col-7">\
                            <b>Class of Job Category</b>\
                          </div>\
                          <div class="col-5">\
                          '+job_category+'\
                          </div>\
                        </div>\
                        <div class="row">\
                          <div class="col-7">\
                            <b>Years of working in current job</b>\
                          </div>\
                          <div class="col-5">\
                          '+current_job_years+'\
                          </div>\
                        </div>\
                        <div class="row">\
                          <div class="col-7">\
                            <b>Type of work arrangement</b>\
                          </div>\
                          <div class="col-5">\
                          '+work_type+'\
                          </div>\
                        </div>\
                        <div class="row">\
                          <div class="col-7">\
                            <b>Age</b>\
                          </div>\
                          <div class="col-5">\
                          '+birthday_interval+'\
                          </div>\
                        </div>\
                        <div class="row">\
                          <div class="col-7">\
                            <b>Gender</b>\
                          </div>\
                          <div class="col-5">\
                          '+gender+'\
                          </div>\
                        </div>\
                        <div class="row">\
                          <div class="col-7">\
                            <b>Location of the organization</b>\
                          </div>\
                          <div class="col-5">\
                          '+organization_location+'\
                          </div>\
                        </div>\
                        <div class="row">\
                          <div class="col-7">\
                            <b>Type of employees</b>\
                          </div>\
                          <div class="col-5">\
                          '+employees_type+'\
                          </div>\
                        </div>\
                      </div>\
                      <div class="col-lg-1"></div>\
                    </div> \
                    <div class="row text-dark-green" style="padding-top:80px">\
                      <div class="col-lg-2"></div>\
                      <div class="col-lg-8">\
                        <div class="text-center subheading-font">\
                          <p>'+text+'</p>\
                        </div>\
                      </div>\
                      <div class="col-lg-2"></div>\
                    </div>\
                  </div>';
                  
        res.status(200).send(data);        
      }
    });
  });

  app.get('/user_step2', function(req, res, next) {
    
    var user_code = req.query.user_code;
    var link_id = req.query.link;
    res.render('user/user_step2', {user_code:user_code, link_id:link_id, firm_name:req.session.company_name});

  });

  app.post('/user_answer', function(req, res, next) {
    
    var user_code = req.body.userCode;
    var link_id = req.body.linkCode;
    var organization_years = req.body.organization_years;
    var job_category = req.body.job_category;
    var current_job_years = req.body.current_job_years;
    var work_type = req.body.work_type;
    var birthday = req.body.birthday;
    var gender = req.body.gender;
    var organization_location = req.body.organization_location;
    var employees_type = req.body.employees_type;

    question.readQuestion(req, res, function(err, question){
      if(question){
        res.render('user/user_answer', {user_code:user_code, 
                                        link_id:link_id,
                                        question:question,
                                        organization_years:organization_years,
                                        job_category:job_category,
                                        current_job_years:current_job_years,
                                        work_type:work_type,
                                        birthday:birthday,
                                        gender:gender,
                                        organization_location:organization_location,
                                        employees_type:employees_type,
                                        firm_name:req.session.company_name
                                      });      
      }
    });
  });
}