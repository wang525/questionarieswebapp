var link=require('../models/link');
var master=require('../models/master');
var user=require('../models/user');
var common=require('../models/common');
var payment_method=require('../config/payment_method');

module.exports = function(app) {

  app.get('/plan_buy',function(req, res, next) {

    var numberOfUsers;
    var numberOfMasters;
    var masterCode = new Array();
    var userCode = new Array();

    link.createLink(req, res, function(err, link_data){
      if(link_data){
        master.createMaster(req, res, function(err, master_data){
          if(master_data){
            user.createUser(req, res, function(err, user_data){
              if(user_data){
                numberOfMasters = link_data[0][1];
                numberOfUsers = link_data[0][2];
                for (var i=0; i<numberOfMasters; i++){
                  masterCode[i] = master_data[i][0];
                }
                for (var i=0; i<numberOfUsers; i++){
                  userCode[i] = user_data[i][0];
                }
                // console.log("master code", masterCode);
                // app.mailer.send('plan/email', {
                //   to: req.session.email, // REQUIRED. This can be a comma delimited string just like a normal email to field.  
                //   subject: 'Test Email', // REQUIRED. 
                //   master_code:masterCode,
                //   user_code:userCode
                //   //otherProperty: 'Other Property' // All additional properties are also passed to the template as local variables. 
                // }, function (err) {
                //   if (err) {
                //     // handle error 
                //     console.log(err);
                //     res.send('There was an error sending the email');
                //     return;
                //   }
                //   res.render('plan/plan_buy', { number_master:numberOfMasters, 
                //                                 number_users:numberOfUsers, 
                //                                 master_code:masterCode,
                //                                 user_code:userCode,
                //                                 firm_name:req.session.company_name});
                //   });

                  res.render('plan/plan_buy', { number_master:numberOfMasters, 
                    number_users:numberOfUsers, 
                    master_code:masterCode,
                    user_code:userCode,
                    firm_name:req.session.company_name});
              }
            });
          }
        });
      }
    });
  });
};