var mysql    = require('mysql');

var dbconfig = require('../config/database');

// Script for setting up database and tables
var conn = mysql.createConnection(dbconfig.connection);

conn.query('CREATE DATABASE ' + dbconfig.database);

// Set up link table
conn.query('\
CREATE TABLE `' + dbconfig.database + '`.`link` ( \
  `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT, \
  `session_link` VARCHAR(255) NOT NULL, \
  `number_master` VARCHAR(255) NOT NULL, \
  `number_user` VARCHAR(255) NOT NULL \
)');

// setup master table
conn.query('\
CREATE TABLE `' + dbconfig.database + '`.`master` ( \
  `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT, \
  `code` VARCHAR(255) NOT NULL, \
  `status` VARCHAR(255) NOT NULL, \
  `link_id` VARCHAR(255) NOT NULL, \
  `email` VARCHAR(255) NOT NULL, \
  `position` VARCHAR(255) NOT NULL \
)');

// setup master table
conn.query('\
CREATE TABLE `' + dbconfig.database + '`.`user` ( \
  `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT, \
  `code` VARCHAR(255) NOT NULL, \
  `link_id` VARCHAR(255) NOT NULL \
)');

// setup master table
conn.query('\
CREATE TABLE `' + dbconfig.database + '`.`result` ( \
  `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT, \
  `user_code` VARCHAR(255) NOT NULL, \
  `avg_res` VARCHAR(255) NOT NULL, \
  `json` TEXT NOT NULL, \
  `date` VARCHAR(255) NOT NULL, \
  `op1` VARCHAR(255) NOT NULL, \
  `op2` VARCHAR(255) NOT NULL, \
  `op3` VARCHAR(255) NOT NULL, \
  `op4` VARCHAR(255) NOT NULL, \
  `op5` VARCHAR(255) NOT NULL, \
  `op6` VARCHAR(255) NOT NULL, \
  `op7` VARCHAR(255) NOT NULL, \
  `op8` VARCHAR(255) NOT NULL, \
  `link_id` VARCHAR(255) NOT NULL \
)');

// setup question table
conn.query('\
CREATE TABLE `' + dbconfig.database + '`.`question` ( \
  `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT, \
  `question` VARCHAR(255) NOT NULL, \
  `min` VARCHAR(255) NOT NULL, \
  `max` VARCHAR(255) NOT NULL, \
  `mark` VARCHAR(255) NOT NULL \
)');


// setup result for master table
conn.query('\
CREATE TABLE `' + dbconfig.database + '`.`result_master` ( \
  `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT, \
  `master_id` VARCHAR(255) NOT NULL, \
  `op1` VARCHAR(255) NOT NULL, \
  `op2` VARCHAR(255) NOT NULL, \
  `op3` VARCHAR(255) NOT NULL, \
  `op4` VARCHAR(255) NOT NULL, \
  `op5` VARCHAR(255) NOT NULL, \
  `op6` VARCHAR(255) NOT NULL, \
  `op7` VARCHAR(255) NOT NULL, \
  `op8` VARCHAR(255) NOT NULL, \
  `result` VARCHAR(255) NOT NULL, \
  `number_users` VARCHAR(255) NOT NULL, \
  `date` VARCHAR(255) NOT NULL \
)');

var sql = 'INSERT INTO `' + dbconfig.database + '`.`question` ( question, min, max, mark ) values ?';
var params = [
  ['Leaders and senior management in my organization tolerate deviation from the stated core values', 
  'Yes, indeed - there is high tolerance (allowing lots of flexibility and interpretations', 
  'Not at all - there is low tolerance (people can even be fired or severely punished)',
  ' '],
  ['leaders in my firm behave and role model the stated core values',
  'Rarely', 
  'Always ro all the time',
  ' '],
  ['At difficult times (i.e.  crisis,  conflict,  cash flow problems) stated core values are being  ignored  in my organization.',
  'Always ignored', 
  'Never ignored',
  ' '],
  ['Regardless of stated core values,  the day to day activities in my firm is focused  more on profits and efficiency rather than the mission/purpose of the organization ..',
  'Profit/efficiency only', 
  'Mission/purpose always',
  ' '],
  ['Policies and practices in the organization are aligned with the stated core values.',
  'Not aligned at all', 
  'Extremely aligned',
  ' '],
  ['The organization  invests resources (training,  communication, etc.) to connect member s behavior with the stated core values.',
  'Not really', 
  'Indeed, to a great deal',
  ' '],
  ['The organization  is open to refresh and examine periodically its core values.',
  'Yes, but only with top management', 
  'Yes, and applies to all members of the firm',
  ' '],
  ['The stated core values in my organization resonate positively.',
  'Dogmatic - no changes are allowed', 
  'Flexible -open to new ideas',
  ' '],
  ['All hierarchical levels in the firm practice coaching and teaching of the stated core values.',
  'Only a few are doing it', 
  'All managers and leaders are doing it',
  ' '],
  ['The stated core values of my company are empowering (clear and invigorating)',
  'Not at all empowering', 
  'Very empowering',
  ' ']
];

conn.query(sql, [params]);

console.log('Success! Database created.');

conn.end();
